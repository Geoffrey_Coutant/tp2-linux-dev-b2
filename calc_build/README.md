# Pour build le container

```
docker build -t calc .
```


# Pour lancer l'application

Directement
```
docker run -e CALC_PORT={port} calc
```


## Et avec docker compose

```
docker compose up
```