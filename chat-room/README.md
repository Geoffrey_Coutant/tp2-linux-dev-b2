# Build le container

```
docker build -t calc .
```

# Lancer l'application directement

```
docker run -e CALC_PORT={port} calc
```

# Lancer l'application avec Docker Compose

```
docker compose up
```
